use reqwest::Client;
use serde_json::json;

pub struct DiscordMessage {
    pub message: String,
    pub url: String,
    pub username: String,
    pub tts: bool,
    pub avatar_url: Option<String>,
}

impl DiscordMessage {
    const RESTRICTED_USERNAMES: [&'static str; 5] =
        ["Clyde", "Webhook", "System", "Everyone", "Here"];

    pub fn new(
        message: String,
        url: String,
        username: String,
        tts: bool,
        avatar_url: Option<String>,
    ) -> Result<Self, String> {
        let lower_username = username.to_lowercase();

        if Self::RESTRICTED_USERNAMES
            .iter()
            .any(|&u| u.eq_ignore_ascii_case(&lower_username))
        {
            return Err(format!(
                "The username '{}' is restricted and cannot be used.",
                username
            ));
        }

        Ok(Self {
            message,
            url,
            username,
            tts,
            avatar_url,
        })
    }
}

pub async fn send_webhook_message(data: &DiscordMessage) -> Result<(), reqwest::Error> {
    let client = Client::new();
    let mut payload = json!({
        "content": data.message,
        "username": data.username,
        "tts": data.tts
        // "embeds": [
            //    {
            //        "title": "Title",
            //        "description": "Description",
            //        "url": "https://example.com",
            //        "color": 15258703,
            //        "fields": [
            //            {"name": "Field 1", "value": "Some value", "inline": False},
            //            {"name": "Field 2", "value": "Another value", "inline": True},
            //        ],
            //        "author": {
            //            "name": "Author Name",
            //            "url": "https://author-url.com",
            //            "icon_url": "https://example.com/author-icon.jpg",
            //        },
            //        "footer": {
            //            "text": "Footer text",
            //            "icon_url": "https://example.com/footer-icon.jpg",
            //        },
            //    }
            // ],
            // "allowed_mentions": {
            //    "parse": ["users", "roles"],
            //    "users": ["user_id1", "user_id2"],
            //    "roles": ["role_id1", "role_id2"],
            //    "replied_user": True,
            // },
    });

    if let Some(avatar_url) = &data.avatar_url {
        payload["avatar_url"] = json!(avatar_url);
    }

    let res = client
        .post(&data.url)
        .json(&payload)
        .send()
        .await?
        .error_for_status()?;

    Ok(())
}
