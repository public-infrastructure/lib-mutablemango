use std::fs;
use std::io::{self, BufRead};

// Function to remove comments and store in memory
pub fn remove_comments(file_path: &str) -> io::Result<String> {
    let file = fs::File::open(file_path)?;
    let reader = io::BufReader::new(file);
    let filtered: Vec<String> = reader
        .lines()
        .filter_map(|line| line.ok())
        .map(|line| {
            let parts: Vec<&str> = line.splitn(2, '#').collect();
            parts[0].trim().to_string()
        })
        .filter(|line| !line.is_empty())
        .collect();

    Ok(filtered.join("\n"))
}

//
//
// TESTS
//
//
#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Write;
    use tempfile::NamedTempFile;

    #[test]
    fn test_remove_comments_normal_case() {
        let mut temp_file = NamedTempFile::new().expect("Failed to create temp file");
        writeln!(temp_file, "line1\nline2 # comment\nline3").unwrap();

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, "line1\nline2\nline3");
    }

    #[test]
    fn test_remove_comments_no_comments() {
        let mut temp_file = NamedTempFile::new().expect("Failed to create temp file");
        writeln!(temp_file, "line1\nline2\nline3").unwrap();

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, "line1\nline2\nline3");
    }

    #[test]
    fn test_remove_comments_only_comments() {
        let mut temp_file = NamedTempFile::new().expect("Failed to create temp file");
        writeln!(temp_file, "#comment1\n#comment2\n#comment3").unwrap();

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, ""); // Should return empty string
    }

    #[test]
    fn test_remove_comments_mixed_content() {
        let mut temp_file = NamedTempFile::new().expect("Failed to create temp file");
        writeln!(
            temp_file,
            "line1\n# full comment line\nline2 # end comment\n   \nline3   "
        )
        .unwrap();

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, "line1\nline2\nline3");
    }

    #[test]
    fn test_remove_comments_empty_file() {
        let temp_file = NamedTempFile::new().expect("Failed to create temp file");

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, ""); // Should return empty string
    }

    #[test]
    fn test_remove_comments_spaces_and_blanks() {
        let mut temp_file = NamedTempFile::new().expect("Failed to create temp file");
        writeln!(temp_file, "   \n   \nline1  \n #comment").unwrap();

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, "line1");
    }

    #[test]
    fn test_remove_comments_special_characters() {
        let mut temp_file = NamedTempFile::new().expect("Failed to create temp file");
        writeln!(temp_file, "hello #comment\nこんにちは # コメント\n").unwrap();

        let result = remove_comments(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(result, "hello\nこんにちは");
    }

    #[test]
    fn test_remove_comments_invalid_file() {
        let result = remove_comments("/nonexistent/path");
        assert!(result.is_err()); // Should return an error
    }
}
