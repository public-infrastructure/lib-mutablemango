use once_cell::sync::Lazy;
use std::net::ToSocketAddrs;
use std::thread::sleep;
use std::time::Duration;

#[derive(Debug)]
pub struct InternetInfo {
    pub has_internet: bool,
    pub times_waited: u32,
    pub wait_interval: u32,
    pub stopped_waiting: bool,
}

pub struct InternetRequirements {
    pub host: String,
    pub port: u16,
    pub max_wait: u64,
    pub wait_interval: u64,
    pub extra_wait: u64,
    pub timeout: u64,
}

// Define a global constant for default settings
pub static DEFAULT_INTERNET_REQUIREMENTS: Lazy<InternetRequirements> =
    Lazy::new(|| InternetRequirements {
        host: "google.com".to_string(),
        port: 80,
        max_wait: 86400,
        wait_interval: 5,
        extra_wait: 2,
        timeout: 3,
    });

fn _internet(host: &str, port: u16, timeout: u64) -> bool {
    let address = format!("{}:{}", host, port);

    match address.to_socket_addrs() {
        Ok(mut addrs) => {
            if let Some(socket_addr) = addrs.next() {
                return std::net::TcpStream::connect_timeout(
                    &socket_addr,
                    std::time::Duration::from_secs(timeout),
                )
                .is_ok();
            } else {
                println!("Resolved, but no valid address found.");
            }
        }
        Err(e) => {
            println!("Address parsing failed: {:?}", e); // Print the exact error
        }
    }

    false
}

pub fn is_internet_reachable(reqs: &InternetRequirements) -> InternetInfo {
    is_internet_reachable_with(reqs, &_internet)
}

// This is only used for testing
pub fn is_internet_reachable_with(
    reqs: &InternetRequirements,
    check_internet_fn: &(dyn Fn(&str, u16, u64) -> bool + Send + Sync),
) -> InternetInfo {
    let mut times_waited = 0;
    let mut has_internet = false;
    let mut stopped_waiting = false;

    let max_wait_times = if reqs.max_wait > 0 {
        reqs.max_wait / reqs.wait_interval.max(1)
    } else {
        u64::MAX // Run indefinitely
    };

    while !check_internet_fn(&reqs.host, reqs.port, reqs.timeout) {
        if reqs.max_wait > 0 && times_waited >= max_wait_times {
            stopped_waiting = true;
            break;
        }
        times_waited += 1;
        sleep(Duration::from_secs(reqs.wait_interval));
    }

    if !stopped_waiting {
        has_internet = true;
    }

    sleep(Duration::from_secs(reqs.extra_wait)); // Extra padding

    InternetInfo {
        has_internet,
        times_waited: times_waited as u32,
        wait_interval: reqs.wait_interval as u32,
        stopped_waiting,
    }
}

//
//
// TESTS
//
//
#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Mutex;
    use std::time::Duration;

    // Mutex to prevent concurrent test failures when mocking
    static INTERNET_MOCK: Mutex<Option<Box<dyn Fn(&str, u16, u64) -> bool + Send + Sync>>> =
        Mutex::new(None);

    // Mock function to override `_internet`
    fn mock_internet<F>(func: F)
    where
        F: Fn(&str, u16, u64) -> bool + Send + Sync + 'static,
    {
        let mut mock = INTERNET_MOCK.lock().unwrap();
        *mock = Some(Box::new(func));
    }

    // Wrapper that calls the actual or mocked `_internet`
    fn _internet_wrapper(host: &str, port: u16, timeout: u64) -> bool {
        if let Some(mock_func) = &*INTERNET_MOCK.lock().unwrap() {
            return mock_func(host, port, timeout);
        }
        _internet(host, port, timeout)
    }

    // Replace the real `_internet` call with `_internet_wrapper` in tests
    #[test]
    fn test_internet_successful_connection() {
        mock_internet(|_, _, _| true); // Always return true

        assert!(_internet_wrapper("example.com", 80, 3));
    }

    #[test]
    fn test_internet_dns_resolution_failure() {
        mock_internet(|_, _, _| false); // Always fail

        assert!(!_internet_wrapper("invalid.domain", 80, 3));
    }

    #[test]
    fn test_internet_unreachable_port() {
        mock_internet(|_, _, _| false); // Simulate a connection timeout

        assert!(!_internet_wrapper("example.com", 9999, 3));
    }

    // ------------------------------------
    // Tests for `is_internet_reachable`
    // ------------------------------------

    #[test]
    fn test_is_internet_reachable_immediately() {
        mock_internet(|_, _, _| true); // Internet is available immediately

        let reqs = InternetRequirements {
            host: "example.com".to_string(),
            port: 80,
            max_wait: 10,
            wait_interval: 1,
            extra_wait: 0,
            timeout: 3,
        };

        let result = is_internet_reachable(&reqs);

        assert!(result.has_internet);
        assert_eq!(result.times_waited, 0);
        assert!(!result.stopped_waiting);
    }

    #[test]
    fn test_is_internet_reachable_timeout() {
        let mock_fn = |_: &str, _: u16, _: u64| false; // Internet never becomes available

        let reqs = InternetRequirements {
            host: "example.com".to_string(),
            port: 80,
            max_wait: 3,
            wait_interval: 1,
            extra_wait: 0,
            timeout: 3,
        };

        let result = is_internet_reachable_with(&reqs, &mock_fn); // Pass reference

        assert!(!result.has_internet);
        assert_eq!(result.times_waited, 3);
        assert!(result.stopped_waiting);
    }

    #[test]
    fn test_is_internet_reachable_after_retries() {
        use std::sync::atomic::{AtomicUsize, Ordering};
        use std::sync::Arc;

        static ATTEMPTS: AtomicUsize = AtomicUsize::new(0);

        let mock_fn: Arc<dyn Fn(&str, u16, u64) -> bool + Send + Sync> = Arc::new(|_, _, _| {
            let attempts = ATTEMPTS.fetch_add(1, Ordering::SeqCst);
            attempts >= 2 // Fail twice, then succeed
        });

        let reqs = InternetRequirements {
            host: "example.com".to_string(),
            port: 80,
            max_wait: 10,
            wait_interval: 1,
            extra_wait: 0,
            timeout: 3,
        };

        let result = is_internet_reachable_with(&reqs, &*mock_fn);

        assert!(result.has_internet);
        assert_eq!(result.times_waited, 2);
        assert!(!result.stopped_waiting);
    }

    #[test]
    fn test_is_internet_reachable_max_wait_zero() {
        use std::sync::atomic::{AtomicUsize, Ordering};
        use std::sync::Arc;

        static ATTEMPTS: AtomicUsize = AtomicUsize::new(0);

        let mock_fn: Arc<dyn Fn(&str, u16, u64) -> bool + Send + Sync> = Arc::new(|_, _, _| {
            let attempts = ATTEMPTS.fetch_add(1, Ordering::SeqCst);
            attempts == 5 // Simulate success after 5 attempts
        });

        let reqs = InternetRequirements {
            host: "example.com".to_string(),
            port: 80,
            max_wait: 0, // Should run indefinitely
            wait_interval: 1,
            extra_wait: 0,
            timeout: 3,
        };

        let result = is_internet_reachable_with(&reqs, &*mock_fn);

        assert!(result.has_internet);
        assert_eq!(result.times_waited, 5);
        assert!(!result.stopped_waiting);
    }

    #[test]
    fn test_is_internet_reachable_extra_wait() {
        mock_internet(|_, _, _| true);

        let reqs = InternetRequirements {
            host: "example.com".to_string(),
            port: 80,
            max_wait: 10,
            wait_interval: 1,
            extra_wait: 2,
            timeout: 3,
        };

        let start_time = std::time::Instant::now();
        let _result = is_internet_reachable(&reqs);
        let elapsed = start_time.elapsed();

        assert!(elapsed >= Duration::from_secs(2)); // Ensures extra_wait delay
    }
}
