use std::error::Error;
use std::process::{Command, Output, Stdio};

pub fn run_command(command: &[&str]) -> Result<Output, Box<dyn Error>> {
    let output = Command::new(command[0])
        .args(&command[1..])
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .output()
        .map_err(|e| {
            Box::new(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Failed to execute '{}': {}", command.join(" "), e),
            ))
        })?;

    if !output.status.success() {
        return Err(Box::new(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "Command '{}' failed: {}",
                command.join(" "),
                String::from_utf8_lossy(&output.stderr)
            ),
        )));
    }

    Ok(output)
}

#[cfg(test)]
mod tests {
    use super::*;

    // Helper to check OS type and return platform-specific commands
    fn get_platform_command(command: &str) -> Vec<&str> {
        if cfg!(target_os = "windows") {
            // On Windows, we use "echo" and "dir"
            match command {
                "echo" => vec!["echo", "Hello, World!"],
                "dir" => vec!["dir"],
                _ => vec![command], // Return the command as is for failure cases
            }
        } else {
            // On Unix-like systems, we use "echo" and "ls"
            match command {
                "echo" => vec!["echo", "Hello, World!"],
                "ls" => vec!["ls", "-l"],
                _ => vec![command], // Return the command as is for failure cases
            }
        }
    }

    // Test for a simple successful echo command
    #[test]
    fn test_run_command_echo_success() {
        let command = get_platform_command("echo");

        let result = run_command(&command);

        assert!(result.is_ok()); // Should succeed
        let output = result.unwrap();
        assert!(String::from_utf8_lossy(&output.stdout).contains("Hello, World!"));
        // Expect output to contain "Hello, World!"
    }

    // Test for a command that will fail (e.g., running a non-existent command)
    #[test]
    fn test_run_command_failure() {
        let command = get_platform_command("nonexistent_command"); // A command that doesn't exist on either platform

        let result = run_command(&command);

        assert!(result.is_err()); // Should fail
        if let Err(e) = result {
            assert!(e.to_string().contains("Failed to execute"));
        }
    }

    // Test for listing files (e.g., 'ls' on Linux or 'dir' on Windows)
    #[test]
    fn test_run_command_list_files() {
        let command = get_platform_command("ls"); // or "dir" for Windows

        let result = run_command(&command);

        assert!(result.is_ok()); // Should succeed
        let output = result.unwrap();
        assert!(String::from_utf8_lossy(&output.stdout).contains("total")); // Should list files in the current directory
    }
}
