use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::process::{Command, ExitStatus};

#[derive(Debug, Clone)]
pub struct Disk {
    pub name: String,
    pub mount_location: String,
    pub in_fstab: bool,
    pub device: Option<String>,
    pub filesystem_type: Option<String>,
    pub depends: Vec<String>,
    pub options: Vec<String>,
}

impl Disk {
    pub fn new(
        name: String,
        mount_location: String,
        in_fstab: bool,
        device: Option<String>,
        filesystem_type: Option<String>,
        depends: Vec<String>,
        options: Vec<String>,
    ) -> Result<Self, String> {
        if !in_fstab {
            if device.is_none() {
                return Err(format!(
                    "Device is required for disk '{}' when in_fstab is false.",
                    name
                ));
            }
            if filesystem_type.is_none() {
                return Err(format!(
                    "Filesystem type is required for disk '{}' when in_fstab is false.",
                    name
                ));
            }
        }

        Ok(Self {
            name,
            mount_location,
            in_fstab,
            device,
            filesystem_type,
            depends,
            options,
        })
    }
}

// Public method that automatically passes in the real mount command
pub fn mount_disk(disk: &Disk) -> Result<(), String> {
    mount_disk_with_fn(disk, |cmd| {
        Command::new(cmd[0].clone())
            .args(&cmd[1..])
            .status()
            .map_err(|e| format!("Failed to execute mount command: {}", e))
    })
}

fn mount_disk_with_fn<F>(disk: &Disk, mount_exec_fn: F) -> Result<(), String>
where
    F: Fn(&Vec<String>) -> Result<ExitStatus, String>,
{
    let mut mount_cmd = vec!["mount".to_string()]; // Always "mount"

    if disk.in_fstab {
        // If the entry is already in /etc/fstab, just mount the location
        mount_cmd.push(disk.mount_location.clone());
    } else {
        // Ensure required fields are set (already validated in `Disk::new`)
        let device = disk.device.as_ref().ok_or_else(|| {
            format!(
                "Device is required for disk '{}' when in_fstab is false.",
                disk.name
            )
        })?;
        let filesystem_type = disk.filesystem_type.as_ref().ok_or_else(|| {
            format!(
                "Filesystem type is required for disk '{}' when in_fstab is false.",
                disk.name
            )
        })?;

        // Check if the mount point exists
        if !Path::new(&disk.mount_location).exists() {
            return Err(format!(
                "Mount point {} does not exist",
                disk.mount_location
            ));
        }

        // Construct the mount command
        if !disk.options.is_empty() {
            mount_cmd.push("-o".to_string());
            mount_cmd.push(disk.options.join(",")); // Join options with commas
        }

        if filesystem_type == "bind" {
            mount_cmd.push("--bind".to_string());
        } else {
            mount_cmd.push("-t".to_string());
            mount_cmd.push(filesystem_type.clone());
        }

        // Add device and mount location
        mount_cmd.push(device.clone());
        mount_cmd.push(disk.mount_location.clone());
    }

    // Execute the mount command using the provided function
    let status = mount_exec_fn(&mount_cmd)?;

    if status.success() {
        Ok(())
    } else {
        Err(format!(
            "Mount command failed with exit code: {}",
            status.code().unwrap_or(-1)
        ))
    }
}

// Sorts dependencies lexicographically to ensure stable and deterministic order
fn topological_sort(disks: Vec<Disk>) -> Result<Vec<Disk>, String> {
    let disk_dict: HashMap<String, Disk> = disks
        .into_iter()
        .map(|disk| (disk.name.clone(), disk))
        .collect();

    let mut visited = HashSet::new();
    let mut sorted_disks = Vec::new();
    let mut temp_mark = HashSet::new();

    fn visit(
        disk_name: &str,
        disk_dict: &HashMap<String, Disk>,
        visited: &mut HashSet<String>,
        sorted_disks: &mut Vec<Disk>,
        temp_mark: &mut HashSet<String>,
    ) -> Result<(), String> {
        if temp_mark.contains(disk_name) {
            return Err(format!("Circular dependency detected: {}", disk_name));
        }
        if !visited.contains(disk_name) {
            temp_mark.insert(disk_name.to_string());
            let disk = disk_dict
                .get(disk_name)
                .ok_or_else(|| format!("Unknown disk: {}", disk_name))?;

            // Sort dependencies to ensure deterministic processing order
            let mut dependencies = disk.depends.clone();
            dependencies.sort();

            for dep in dependencies {
                visit(&dep, disk_dict, visited, sorted_disks, temp_mark)?;
            }

            temp_mark.remove(disk_name);
            visited.insert(disk_name.to_string());
            sorted_disks.push(disk.clone());
        }
        Ok(())
    }

    // Sort disk names to process them in a consistent order
    let mut disk_names: Vec<String> = disk_dict.keys().cloned().collect();
    disk_names.sort();

    for disk_name in disk_names {
        if !visited.contains(&disk_name) {
            visit(
                &disk_name,
                &disk_dict,
                &mut visited,
                &mut sorted_disks,
                &mut temp_mark,
            )?;
        }
    }

    Ok(sorted_disks)
}

pub fn mount_disks(disks: Vec<Disk>) -> Result<(), String> {
    let sorted_disks = topological_sort(disks)?; // Sort disks based on dependencies

    for disk in sorted_disks {
        mount_disk(&disk).map_err(|e| format!("Failed to mount {}: {}", disk.name, e))?;
    }

    Ok(())
}

//
//
// TESTS
//
//
#[cfg(test)]
mod tests {
    use super::*;
    use std::os::unix::process::ExitStatusExt;
    use std::process::ExitStatus;

    // Mock function to simulate successful mount execution
    fn mock_mount_exec_fn(_cmd: &Vec<String>) -> Result<ExitStatus, String> {
        Ok(ExitStatus::from_raw(0)) // Simulate success
    }

    // Mock function to simulate a failure in mount execution
    fn mock_mount_exec_fn_fail(_cmd: &Vec<String>) -> Result<ExitStatus, String> {
        // Simulate mount point non-existence
        Err("Mount command failed with exit code: 1".to_string()) // Simulate failure
    }

    // Mock function to simulate mount point not existing
    fn mock_mount_exec_fn_mount_point_not_exist(cmd: &Vec<String>) -> Result<ExitStatus, String> {
        Err(format!("Mount point {} does not exist", cmd[2]))
    }

    // Test successful mount when disk is in fstab
    #[test]
    fn test_mount_disk_in_fstab() {
        let disk = Disk {
            name: "test_disk".to_string(),
            mount_location: "/mnt/test".to_string(),
            in_fstab: true,
            device: None,
            filesystem_type: None,
            depends: vec![],
            options: vec!["ro".to_string()],
        };

        let result = mount_disk_with_fn(&disk, mock_mount_exec_fn);
        assert!(result.is_ok());
    }

    // Test successful mount when disk is not in fstab (with valid fields)
    #[test]
    fn test_mount_disk_not_in_fstab() {
        let disk = Disk::new(
            "test_disk".to_string(),
            "/".to_string(),
            false,
            Some("/dev/sda1".to_string()),
            Some("ext4".to_string()),
            vec![],
            vec![],
        )
        .unwrap();

        // Test with mock function that simulates a successful mount
        let result = mount_disk_with_fn(&disk, mock_mount_exec_fn);
        assert!(result.is_ok()); // Should pass if the mount was successful
    }

    // Test when required fields (device or filesystem type) are missing when not in fstab
    #[test]
    fn test_mount_disk_missing_device_or_filesystem_type() {
        let disk_missing_device = Disk {
            name: "test_disk".to_string(),
            mount_location: "/mnt/test".to_string(),
            in_fstab: false,
            device: None,
            filesystem_type: Some("ext4".to_string()),
            depends: vec![],
            options: vec!["ro".to_string()],
        };

        let result = mount_disk_with_fn(&disk_missing_device, mock_mount_exec_fn);
        assert!(result.is_err());
        assert_eq!(
            result.err().unwrap(),
            "Device is required for disk 'test_disk' when in_fstab is false."
        );

        let disk_missing_filesystem = Disk {
            name: "test_disk".to_string(),
            mount_location: "/mnt/test".to_string(),
            in_fstab: false,
            device: Some("/dev/sda1".to_string()),
            filesystem_type: None,
            depends: vec![],
            options: vec!["ro".to_string()],
        };

        let result = mount_disk_with_fn(&disk_missing_filesystem, mock_mount_exec_fn);
        assert!(result.is_err());
        assert_eq!(
            result.err().unwrap(),
            "Filesystem type is required for disk 'test_disk' when in_fstab is false."
        );
    }

    // Test when the mount point does not exist
    #[test]
    fn test_mount_disk_mount_point_not_exist() {
        let disk = Disk {
            name: "test_disk".to_string(),
            mount_location: "/mnt/nonexistent".to_string(),
            in_fstab: false,
            device: Some("/dev/sda1".to_string()),
            filesystem_type: Some("ext4".to_string()),
            depends: vec![],
            options: vec!["ro".to_string()],
        };

        let result = mount_disk_with_fn(&disk, mock_mount_exec_fn_mount_point_not_exist);
        assert!(result.is_err());
        assert_eq!(
            result.err().unwrap(),
            "Mount point /mnt/nonexistent does not exist"
        );
    }

    // Test mount command failure (non-zero exit status)
    #[test]
    fn test_mount_disk_command_failure() {
        let disk = Disk::new(
            "test_disk".to_string(),
            "/".to_string(),
            false,
            Some("/dev/sda1".to_string()),
            Some("ext4".to_string()),
            vec![],
            vec![],
        )
        .unwrap();

        // Test with mock function that simulates a mount failure
        let result = mount_disk_with_fn(&disk, mock_mount_exec_fn_fail);
        assert!(result.is_err()); // Should fail if the mount fails
        assert_eq!(
            result.err().unwrap(),
            "Mount command failed with exit code: 1"
        );
    }

    // Helper function to create a Disk for the topological sort tests
    fn create_disk(name: &str, depends: Vec<String>) -> Disk {
        Disk {
            name: name.to_string(),
            mount_location: format!("/mnt/{}", name),
            in_fstab: true,
            device: None,
            filesystem_type: None,
            depends,
            options: Vec::new(),
        }
    }

    #[test]
    fn test_topological_sort_no_dependencies() {
        let disks = vec![
            create_disk("disk1", vec![]),
            create_disk("disk2", vec![]),
            create_disk("disk3", vec![]),
        ];

        let sorted = topological_sort(disks).unwrap();
        let sorted_names: Vec<String> = sorted.iter().map(|d| d.name.clone()).collect();

        assert_eq!(sorted_names, vec!["disk1", "disk2", "disk3"]);
    }

    #[test]
    fn test_topological_sort_single_dependency() {
        let disks = vec![
            create_disk("disk1", vec![]),
            create_disk("disk2", vec!["disk1".to_string()]),
        ];

        let sorted = topological_sort(disks).unwrap();
        let sorted_names: Vec<String> = sorted.iter().map(|d| d.name.clone()).collect();

        assert_eq!(sorted_names, vec!["disk1", "disk2"]);
    }

    #[test]
    fn test_topological_sort_multiple_dependencies() {
        let disks = vec![
            create_disk("disk1", vec![]),
            create_disk("disk2", vec!["disk1".to_string()]),
            create_disk("disk3", vec!["disk1".to_string(), "disk2".to_string()]),
        ];

        let sorted = topological_sort(disks).unwrap();
        let sorted_names: Vec<String> = sorted.iter().map(|d| d.name.clone()).collect();

        assert_eq!(sorted_names, vec!["disk1", "disk2", "disk3"]);
    }

    #[test]
    fn test_topological_sort_circular_dependency() {
        let disks = vec![
            create_disk("disk1", vec!["disk2".to_string()]),
            create_disk("disk2", vec!["disk1".to_string()]),
        ];

        let result = topological_sort(disks);
        assert!(result.is_err());
        assert_eq!(result.err().unwrap(), "Circular dependency detected: disk1");
    }

    #[test]
    fn test_topological_sort_empty() {
        let disks: Vec<Disk> = vec![];

        let sorted = topological_sort(disks).unwrap();
        assert_eq!(sorted.len(), 0);
    }
}
